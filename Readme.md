# Nginx and Docker
We will learn what nginx is and how to use nginx to serve a nodejs application with multiple replicas running in Docker

## Tutorial reference
https://www.youtube.com/watch?v=q8OleYuqntY

# What we will archieve
1 - Dockerize nodejs application
2 - Run 3 web servers
3 - Install and configure nginx to loadbalance request to our 3 Backend web servers
4 - Configure secure https for our application

# Using buildpack to build an image for the backend

pack build backend --path backend --builder gcr.io/buildpacks/builder:google-22

# Run the container
docker run -it --rm  -p 8080:8080 backend

## configure buildpack to use a build image

Check builder suggestion
> pack builder suggest

TIP: If you don’t want to keep specifying a builder every time you build, you can set it as your default builder by running pack config default-builder <BUILDER> for example pack config default-builder cnbs/sample-builder:noble

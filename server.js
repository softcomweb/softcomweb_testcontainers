const express = require('express');
const path = require('path');
const app = express();
const port = process.env.PORT || 8080;

app.use('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'webapp', 'index.html'));
    console.log('Serving index.html');
});

app.listen(port, () => {
    console.log(`Server listening on port ${port}`);
});
